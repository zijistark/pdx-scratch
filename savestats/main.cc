
#include "pdx/pdx.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>


using namespace std;
using namespace boost::filesystem;
namespace po = boost::program_options;


int main(int argc, const char** argv) {
    try {
        path opt_game_path;
        path opt_asrotor_path;
        string opt_name;

        /* command-line & configuration file parameter specification */

        po::options_description opt_spec{ "Options" };
        opt_spec.add_options()
            ("help,h", "Show help information")
            ("cfg,c",
                po::value<path>(),
                "Configuration file")
            ("game-path",
                po::value<path>(&opt_game_path)->default_value("C:/Program Files (x86)/Steam/steamapps/common/Crusader Kings II"),
                "Path to game folder")
            ("mod-path",
                po::value<path>()->default_value("C:/Users/ziji/Documents/Paradox Interactive/Crusader Kings II/mod/Historical Immersion Project"),
                "Path to root folder of a mod")
            ("submod-path",
                po::value<path>(),
                "Path to root folder of a sub-mod")
            ("asrotor-path",
                po::value<path>(&opt_asrotor_path)->default_value("C:/Users/ziji/Documents/asrotor"),
                "Path to game folder")
            ("name,n",
                po::value<string>(&opt_name)->required(),
                "Name of savegame series")
            ;

        /* parse command line & optional configuration file (command-line options override --config file options)
         *
         * example config file contents:
         *
         *   game-path = C:/SteamLibrary/steamapps/common/Crusader Kings II
         *   mod-path  = D:\git\SWMH-BETA\SWMH
         */

        po::variables_map opt;
        po::store(po::parse_command_line(argc, argv, opt_spec), opt);

        if (opt.count("cfg")) {
            const string cfg_path = opt["cfg"].as<path>().string();
            std::ifstream f_cfg{ cfg_path };

            if (f_cfg)
                po::store(po::parse_config_file(f_cfg, opt_spec), opt);
            else
                throw runtime_error("failed to open config file specified with --cfg: " + cfg_path);
        }

        if (opt.count("help")) {
            cout << opt_spec << endl;
            return 0;
        }

        po::notify(opt);

        pdx::vfs vfs{ opt_game_path };

        if (opt.count("mod-path")) {
            vfs.push_mod_path(opt["mod-path"].as<path>());

            if (opt.count("submod-path"))
                vfs.push_mod_path(opt["submod-path"].as<path>());
        }
        else if (opt.count("submod-path"))
            throw runtime_error("cannot specify --submod-path without also providing a --mod-path");

        /* done with program option processing */

        path sgs_dir = opt_asrotor_path / opt_name;

        vector<path> sgs_files;

        for (auto&& e : directory_iterator(sgs_dir)) {
            path p = e.path();

            if (p.extension() == ".gz" && p.stem().extension() == ".ck2")
                sgs_files.emplace_back(p.filename());
        }

        if (sgs_files.empty())
            throw runtime_error("no savegames found in folder: " + sgs_dir.string());

        sort(begin(sgs_files), end(sgs_files));

        path f = sgs_files.back();
        path unzipped_path = sgs_dir / f.stem();

        if (!exists(unzipped_path)) {

            std::string cmd = "gunzip -k \"" + (sgs_dir / f).string() + '\"';
            cout << "$ " << cmd << endl;

            int rc = system(cmd.c_str());

            if (rc)
                throw va_error("gunzip failed with return code %d", rc);
        }

        assert( exists(unzipped_path) );

        pdx::parser parse(unzipped_path, true);
    }
    catch (const exception& e) {
        cerr << "fatal: " << e.what() << endl;
        return 1;
    }

    return 0;
}
